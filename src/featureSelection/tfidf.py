#!/usr/bin/python
# -*- coding:utf-8 -*-
# author: Jerry.Shi

from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
import pprint
import  numpy as np

if __name__ == '__main__':
    data = [
        "NBA2K16 视频 设置 存储 位置 NBA 视频 设置 存储 位置 解析 攻略 玩游戏",
        "NBA2K16 ncaa 豪门 大学 选择 推荐 NBA ncaa 大学 选择 游戏网 攻略",
        "NBA2K16 学好 NBA2K16 大学 名校 选择 攻略 攻略 心得 单机"
    ]

    # convert corpus to termfreq matrix, tf = a[i][j] , the term frequency of word j in document i
    vectorization = CountVectorizer()
    # Transform a count matrix to a normalized tf or tf-idf representation

    # [1] construct matrix
    transformer = TfidfTransformer()
    tfMatrix = vectorization.fit_transform(data)
    print('\n==============Doc-Term Matrix=====================\n')
    # doc-term matrix
    pprint.pprint(tfMatrix.toarray())

    words = vectorization.get_feature_names()
    # tfArray = tfMatrix.toarray()
    # print all words in corpus
    for j in range(len(words)):
        print words[j]
    # for i in range(len(tfArray)):
    #     for j in range(len(words)):
    #         print words[j]

    # [2] use TF-IDF values to fill the doc-term matrix
    print('\n==================Doc-Term Matrix with TF-IDF values=====================\n')
    data_tfidf = transformer.fit_transform(tfMatrix)
    pprint.pprint(data_tfidf.toarray())

    # [3] dimensionality reduction
    # svd , M(n*m) = U(n*n) * Sigma(n*m) * V(m*m)
    u, sigma, v = np.linalg.svd(data_tfidf.toarray())

    print '============== U ===================\n'
    pprint.pprint(u)
    print '============== sigma ===================\n'
    # there are only three singular value > 0
    pprint.pprint(sigma)
    print '============== U ===================\n'
    pprint.pprint(v)


    # [4] reconstruct doc-term matrix
    # newMatrx(m,n) = U(m,3)*Sigma(3*3)*V(3*n)
    # at this mount , there are only three singular in sigma, so we only need 3 columns in U = U(m,3),
    # 3 rows in V =(3,n), and the result M(m,n) ~= U(m,3)*Sigma(3,3)*V(3, n)
    newSigma = np.mat([[sigma[0],0,0],[0,0,sigma[1]],[0,0,sigma[2]]])
    newMatrix = u[:,:3]*newSigma*v[:3,:]
    print '============== Reconstruction Matrix ===================\n'
    pprint.pprint(newMatrix)



