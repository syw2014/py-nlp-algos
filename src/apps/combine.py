#! /usr/bin python
# -*- coding: utf-8 -*-
# Author: Jerry.shi
# Date: 2017-02-15 14:38:32

# Description:
#   将三级类目数据转换为二级和一级下，目的是扩大分类面，减少精细度。比如： A > B > C，是一个三级类目， 处理结果的数据是 A > B
# 和 A 下.

import sys

def load(cate_file):
    """
    Mapping category file load, format as: cate_id1 \t 3_level_cate \t cate_id2 \t 2_level_cate \t cate_id3 \t 1_level
    :param cate_file: input category mapping file
    :return: cate_id3 and other cate information list[4] map
    lsit[0] : cate_id2 , list[1]: 2_level_cate, list[2]: cate_id3, list[3]: 3_level_cate
    """
    if len(cate_file) == 0 :
        print "Require non empty file but found " + cate_file
        sys.exit(-1)
    # open file stream and process category
    with open(cate_file, 'r') as ifs:
        cate_id = {}
        for line in ifs.readlines():
            # other two level category and it's id
            cateList = []
            line = line.strip()
            cxt = line.split("\t")
            if len(cxt) < 6 :
                continue
            # store category id and category
            # insert list element to cateList except the original category id
            cateList[len(cateList) : len(cateList)] = cxt[2:]
            # print cateList
            cate_id[int(cxt[0].strip())] = cateList
        print("Category loaded, total size: %d") % len(cate_id)
    return cate_id

#
def run(cate_id, samples, twoLevelCateSample, oneLevelCateSample):
    """
    Main process, convert every sample two different levels, then write those datas in related files
    :param cate_id: category id and mapping ids
    :param samples: training samples, format: category_id \t text \t weight
    :param twoLevelCateSample: generated two level category samples, format: category_id \t text \t weight
    :param threeLevelSample: generated one level category samples, format: category_id \t text \t weight
    :return: none
    """
    # check
    if(len(samples) == 0) :
        print("Require none empty data file but found: %s len: %d", samples, len(samples))
        sys.exit(-1)
    # open file stream
    with open(samples, 'r') as ifs, open(twoLevelCateSample, "w") as ofs1, open(oneLevelCateSample, 'w') as ofs2:
        for line in ifs.readlines():
            line = line.strip()
            cxt = line.split("\t")
            if(len(cxt) != 3) :
                continue
            # get labeled id
            cateId = int(cxt[0].strip())
            if cate_id.has_key(cateId) :
                # found mapping category and id
                if(len(cate_id[cateId]) < 4) :
                    print("Require two mapping id but found one: %d", cateId)
                    continue
                # two level category id
                twoLevelID = cate_id[cateId][0]
                # # two level category
                # twoLevelCate = cateId[cateId][1]
                # one category id
                oneLevelID = cate_id[cateId][2]
                # # one levgel category
                # oneLevelCate = cate_id[cateId][3]

                ofs1.write(str(twoLevelID) + "\t" + cxt[1] + "\t0.0")
                ofs1.write('\n')
                ofs2.write(str(oneLevelID) + "\t" + cxt[1] + "\t0.0")
                ofs2.write('\n')

# process predict data
def predictSampleConvt(cateFile, preSample, twoLevelPre, oneLevelPre):
    """
    Convert predict sample to one level class and two level class
    :param cateFile: category mapping file,
    :param preSample: predict samples, format sample_id \t category \t text
    :param twoLevelPre: two level category sample
    :param oneLevelPre: one level category sample
    :return: no specific return,if run succeed it will generate two files in input path.
    """
    if (len(cateFile) == 0):
        print("Require non-empty category mapping file but found empty: %s", cateFile)
        sys.exit(-1)
    with open(cateFile, 'r') as ifs:
        cate_info = {}
        for line in ifs.readlines():
            line = line.strip()
            cxt = line.split("\t")
            if len(cxt) < 6:
                continue
            infoList = []
            # store category mapping infos
            infoList[len(cxt) : len(cxt)] = cxt[2:]
            cate_info[cxt[1].strip()] = infoList
        print("category mapping info loaded, cate size: %d", len(cate_info))

    # conversion
    with open(preSample, 'r') as preIfs, open(twoLevelPre, 'w') as ofs1, open(oneLevelPre, 'w') as ofs2:
        for line in preIfs.readlines():
            line = line.strip()
            cxt = line.split("\t")

            if len(cxt) < 3:
                continue
            if cate_info.has_key(cxt[1].strip()):
                # get mapping info
                mapInfo = cate_info[cxt[1].strip()]
                # write two level predict sample
                ofs1.write(cxt[0] + "\t" + mapInfo[0] + "\t" + mapInfo[1] + "\t" + cxt[2])
                ofs1.write('\n')
                # write one level predict sample
                ofs2.write(cxt[0] + "\t" + mapInfo[2] + "\t" + mapInfo[3] + "\t" + cxt[2])
                ofs2.write('\n')
        print "Process completed!"



if __name__ == '__main__':
    # get parameter
    cateFile = "D:\\github\\py-nlp-algos\\data\\category_map.txt"
    samples = "E:\\work\\Project\Tasks\\1 TextClassification\\finalModel\\train_sample.txt"

    resultPath = "D:\\github\\py-nlp-algos\\result\\"
    cate_id = load(cateFile)
    run(cate_id, samples, resultPath+"two_level_sample.txt", resultPath+"one_level_sample.txt")
    # preSample = "D:\\github\\py-nlp-algos\\data\\groundTruth_with_label.txt"
    # predictSampleConvt(cateFile, preSample, resultPath+"two_level_pre.txt", resultPath+"one_level_pre.txt")
    print "Processed completed!"
