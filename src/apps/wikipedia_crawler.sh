#!/bin/bash
# Author: Jerry.Shi
# Date: 2017-02-27 14:56:30
# Description:
#     Download wikipedia cn dump data.

# main url of dump data
url="https://dumps.wikimedia.org/zhwiki/20170220/"
date="zhwiki-20170220/"
# what content will be downloaded.
url_list=("$date-pages-articles-multistream.xml.bz2" \
"$date-pages-articles-multistream-index.txt.bz2" \
"$date-flowhistory.xml.bz2" \
"$date-pages-logging.xml.gz" \
"$date-pages-meta-current.xml.bz2" \
"$date-stub-meta-history.xml.gz" \
"$date-abstract.xml"	\
"$date-all-titles.gz" \
"$date-pagelinks.sql.gz" \
"$date-geo_tags.sql.gz" \
"$date-page_props.sql.gz" \
"$date-change_tag.sql.gz" \
"$date-categorylinks.sql.gz" \
"$date-externallinks.sql.gz" \
"$date-iwlinks.sql.gz" \
"$date-protected_titles.sql.gz" \
"$date-templatelinks.sql.gz" \
"$date-redirect.sql.gz" \
"$date-site_stats.sql.gz" \
"$date-user_groups.sql.gz" \
"$date-sites.sql.gz" \
"$date-imagelinks.sql.gz" \
"$date-category.sql.gz" \
"$date-page.sql.gz" \
"$date-page_restrictions.sql.gz" \
"$date-wbc_entity_usage.sql.gz" \
"$date-image.sql.gz" \
"$date-langlinks.sql.gz"
)

# print url list length
echo ${#url_list[@]}

# loop url list
for seed in ${url_list[@]};
do
    torrent=$url$seed
    echo $torrent
    #time wget -p $torrent
done
