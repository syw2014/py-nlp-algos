#! /usr/bin python
# -*- coding: utf-8 -*-
# File: w2v.py
# Author: Jerry.shi
# Date: 2017-03-10 15:15

import gensim
import time

modelPath = "E:\\download\\w2v_train.vector"
start = time.time()
model = gensim.models.Word2Vec.load_word2vec_format(modelPath, encoding="utf-8",binary=False)
end = time.time()
print("Model loaded spended %0.3fs", (end - start))

start = time.time()
model.most_similar("手机三国")
end = time.time()
print("Find similarity words spended %0.3fs", (end - start))

start = time.time()
model.similarity("单机游戏","手机")
end = time.time()
print("similarity calculation spended %0.3fs", (end - start))
