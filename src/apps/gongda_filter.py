#! /usr/bin python
# -*- coding: utf-8 -*-
# File: gongda_filter.py
# Author: Jerry.shi
# Date: 2017-02-16 9:49

def convert(infile, outfile):
    """Convert GongDa data to our format"""
    # load category the same as it in GongDa file
    cate_id = {}
    cate = "D:\github\py-nlp-algos\data\category.txt"
    with open(cate, 'r') as ifs:
        for line in ifs.readlines():
            line = line.strip()
            cxt = line.split('\t')
            if len(cxt) != 2:
                continue
            cate_id[cxt[1]] = cxt[0]
    print("Total label size: %d") % len(cate_id)

    # load predicted result
    with open(infile, 'r') as infs, open(outfile, 'w') as ofs:
        for line in infs.readlines():
            line = line.strip()
            cxt = line.split('\t')
            if(len(cxt) < 5) :
                continue
            # predicted label == original label
            if(cxt[1] == cxt[2]):
                if(cate_id.has_key(cxt[1])):
                    id = cate_id[cxt[1]]
                    ofs.write(id + "\t" + cxt[4] + "\t0.0")
                    ofs.write("\n")
    print "Process completed!"

    # load test data,


if __name__ == '__main__':
     file1 = "D:\\github\\py-nlp-algos\\data\\evaluation.txt"
     file2 = "D:\\github\\py-nlp-algos\\result\\sample.txt"
     convert(infile=file1, outfile= file2)



