#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author  : Jerry.Shi
# File    : user_dict_filter.py
# Date    : 2017/4/22 15:30
# Software: PyCharm Community Edition

import sys
import urllib, urllib2

user_dict = ""  # user define dictionary
norm_dict = ""  # dictionary with nature and nature frequency

url_get_base = "http://api.ltp-cloud.com/analysis/"

with open(user_dict, 'r') as ifs, open(norm_dict, 'w') as ofs:
    for line in ifs.readlines():
        line = line.strip()
        if line.isdigit():
            ofs.write(line + "\t" + "m\t1")
            ofs.write("\n")
            continue
        args = {
            'api_key': 'J1l0Z8k7H5lyPArDOv7MzrisijbnijvUSUAeZred',
            'text': line,
            'pattern': 'pos',
            'format': 'plain'
        }
        result = urllib.urlopen(url_get_base, urllib.urlencode(args))  # POST method
        content = result.read().strip()
        if len(content) == 0:

            ofs.write(line + "\t" + "\t1")
            ofs.write("\n")
            continue
        else:
            # TODO: count nature, choose the maximum number as nature
            cxt = content.split(" ")
            pos = cxt[0].split("_")[1]
            ofs.write("\n")
    print "Process completed!"

