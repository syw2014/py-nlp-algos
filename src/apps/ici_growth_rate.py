#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author  : Jerry.Shi
# File    : ici_growth_rate.py
# PythonVersion: python3.5
# Date    : 2017/4/22 9:56
# Software: PyCharm Community Edition

import os

# to store word and ici value
word_base = {}

base_file = "D:\\download\\ici_data\\0412.txt"
file2 = "D:\\download\\ici_data\\0413.txt"
outfile = "D:\\download\\ici_data\\0412-13.txt"

# load base corpus
with open(base_file, 'r') as ifs:
    for line in ifs.readlines():
        line = line.strip()
        cxt = line.split("\t")
        if len(cxt) != 4:
            continue
        word = cxt[1].strip()
        if word_base.has_key(word):
            continue
        else:
            word_base[word] = float(cxt[2].strip())
word_growth_rate = {}
word_ici_current = {}
with open(file2, 'r') as ifs, open(outfile, 'w') as ofs:
    for line in ifs.readlines():
        line = line.strip()
        cxt = line.split()
        if len(cxt) != 4:
             continue
        word = cxt[1].strip()
        word_ici_current[word] = cxt[2].strip()
        if word_base.has_key(word):
            growth_rate = (float(cxt[2].strip()) - word_base[word]) / word_base[word]
            if growth_rate >= 0:
                word_growth_rate[word] = growth_rate
        else:
            continue
    word_growth_rate = (sorted(word_growth_rate.iteritems(), key= lambda d:d[1], reverse=True))
    #
    # for k, v in word_growth_rate.items():
    #     ofs.write(k + "\t" + str(v))
    #     ofs.write("\n")
    for i in range(len(word_growth_rate)):
        pre_ici = 0.0
        cur_ici = 0.0
        word = word_growth_rate[i][0]
        if word_base.has_key(word):
            pre_ici = word_base[word]
        if word_ici_current.has_key(word):
            cur_ici = word_ici_current[word]
        ofs.write(word + "\t" + str(pre_ici) + "\t"+ cur_ici + "\t" + str(word_growth_rate[i][1]))
        ofs.write("\n")
    print "process completed!"