#! /usr/bin python
# -*- coding: utf-8 -*-
# File: query_label.py
# Author: Jerry.shi
# Date: 2017-03-03 14:58

import sys

file1 = "D:\\github\\py-nlp-algos\\data\\sim_result.txt"
file2 = "D:\\github\\py-nlp-algos\\data\\query_label3.txt"
file3 = "D:\\github\\py-nlp-algos\\result\\query_label3.txt"
with open(file1, 'r') as ifs1, open(file2, 'r') as ifs2, open(file3, 'w') as ofs:
    raw_data = {}
    while True:
        line = ifs1.readline()
        if not line:
            break
        line = line.strip()
        cxt = line.split("\t")
        # if len(cxt) < 2:
        #     # print line, len(cxt)
        #     continue
        print  line
        raw_data[cxt[0].strip()] = cxt[1].strip()
    print len(raw_data)
    for line in ifs2.readlines():
        line = line.strip()
        cxt = line.split('\t')
        # if len(cxt) < 3:
        #     continue
        ids = cxt[0].strip()
        if raw_data.has_key(ids):
            ofs.write(ids + "\t" + raw_data[ids] + "\t" + "\t".join(cxt[1:]))
            ofs.write('\n')
        # else:
        #     print line
