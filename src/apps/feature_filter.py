#! /usr/bin python
# -*- coding: utf-8 -*-
# File: feature_filter.py
# Author: Jerry.shi
# Date: 2017-02-23 10:48

# Description:
#   Fliter features for each label, filter rules:
#   1> length <= 2, eg:
#   2> start with digit, eg: 9日, 2月,
#   3> start and end with special character，eg: <br>
#   4> forbidden words
#   5> filter words, eg: Powered

import sys
import jieba.posseg as pseg

# construct tokenizer
# pseg.initialize

def loadDict(dictPath, stopMap, filterMap):
    if len(dictPath) == 0 :
        print "Require a whole dictionary path, but found empty path: %s" % (dictPath)
        sys.exit(-1)
    stopWords = dictPath + "/stop_words.txt"
    filterWords = dictPath + "/filter_words.txt"
    with open(stopWords, 'r') as ifStopStrem, open(filterWords, 'r') as ifFilterStrem:
        # load stop words
        for line in ifStopStrem.readlines():
            line = line.strip()
            stopMap.append(line)
        stopMap = set(stopMap)
        print("stop words loaded size: %d", len(stopMap))
        # load filter words
        for line in ifFilterStrem.readlines():
            line = line.strip()
            filterMap.append(line)
        filterMap = set(filterMap)
        print ("filter words loaded size: %d", len(filterMap))
    print "Dict loaded!"

def isNeedClean(term, stopMap, filterMap):
    term = term.strip()
    term = term.lstrip()

    # rule1: length < 2
    if len(term) <= 2  :
        return True
    # rule2： term in filter map or stop map
    if term in filterMap:
        return True
    if term in stopMap:
        return True
    # rule3: start with digit
    char1 = term[0]
    if char1.isdigit():
        return True
    elif char1 in stopMap:
        return True
    # rule4: end with special character
    charEnd = term[-1]
    if charEnd in stopMap:
        return True
    elif charEnd in filterMap:
        return True
    # rule5: filter by POS
    words = pseg.cut(term)
    for word, flag in words:
        if flag == 'ns':
            print words
            return True

    return False

if __name__ == "__main__":
    dictPath = "D:\\github\\py-nlp-algos\\data"
    stopMap = []
    filterMap = []
    loadDict(dictPath, stopMap, filterMap)
    data = dictPath + "/label_features.txt"
    outfile = "D:\\github\\py-nlp-algos\\result\\label_feature_norm.txt"
    words = pseg.cut("")
    with open(data, 'r') as ifs , open(outfile, 'w') as ofs:
        for line in ifs.readlines():
            line = line.strip()
            cxt = line.split("\t")
            if len(cxt) < 3:
                continue
            term = cxt[1]
            if isNeedClean(term, stopMap, filterMap):
                continue
            ofs.write(line)
            ofs.write('\n')
    print "Feature filter completed!"