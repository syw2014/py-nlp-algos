#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author  : Jerry.Shi
# File    : cn_dbpedia_avp.py
# PythonVersion: python3.5
# Date    : 2017/5/2 15:05
# Software: PyCharm Community Edition

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import urllib2
import json
import httplib
import time
# add progress bar
from  tqdm import *



# set base api
avp_get_api = "http://knowledgeworks.cn:20313/cndbpedia/api/entityAVP?entity="
# to tell the server there is browse
headers = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0'}

# query = "通神"
# result = urllib2.urlopen(avp_get_api+query)  # get request
# content = result.read().strip()
# jdata = json.loads(content)

game_name = "D:\\github\\py-nlp-algos\\data\\game_name.txt"
game_name_rdf = "D:\\github\\py-nlp-algos\\result\\game_name_rdf.txt"
game_name_enmpty = "D:\\github\\py-nlp-algos\\result\\game_name_empty.txt"

with open(game_name, 'r') as ifs, open(game_name_rdf, 'w') as ofs_rdf, open(game_name_enmpty, 'w') as ofs_none:
    for line in tqdm(ifs.readlines()):
        time.sleep(0.01)
        query = line.strip().rstrip()
        content = ""
        try:
            request = urllib2.Request(avp_get_api+query, headers=headers)
            result = urllib2.urlopen(request)
            content = result.read().strip()
        except httplib.BadStatusLine, urllib2.URLError:
            content = ""

        if len(content) == 0:
            ofs_none.write(query)
            ofs_none.write("\n")
            continue

        json_data = json.loads(content)
        av_list = json_data["av pair"]
        if len(av_list) == 0:
            ofs_none.write(query)
            ofs_none.write("\n")
            continue
        for av in av_list:
            if len(av) != 2:
                continue
            # attribute
            attr = av[0]
            val = av[1]
            nstr = query + u'\t'+ attr + u'\t' + val
            ofs_rdf.write(nstr)
            ofs_rdf.write("\n")




