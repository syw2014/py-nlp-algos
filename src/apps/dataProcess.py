#!/usr/bin/python
# -*- coding:utf-8 -*-
# author: Jerry.Shi

import sys

# def load(input, output):
#     """Load category and data"""
#     ifs = open(input, 'r')
#     ofs = open("D:\github\py-nlp-algos\data\category.txt", 'w')
#     cate_ids = {}
#     cnt = 0
#     while True:
#         line = ifs.readline()
#         if not line:
#             break
#         line = line.strip()
#         cnt += 1
#         ofs.write(str(cnt) + "\t" + line)
#         ofs.write('\n')
#     ifs.close()
#     ofs.close()

def convert(infile, outfile):
    """Convert GongDa data to our format"""
    # load category the same as it in GongDa file
    cate_id = {}
    ifs = open("D:\github\py-nlp-algos\data\category.txt", 'r')
    for line in ifs.readlines():
        line = line.strip()
        cxt = line.split('\t')
        if len(cxt) != 2:
            continue
        cate_id[cxt[0]] = cxt[1]
    print("Total label size: %d") % len(cate_id)
    ifs.close()

    # load data and process
    with open(infile,'r') as ifs, open(output, 'w') as ofs:
        cnt = 0
        for line in ifs.readlines():
            pos_end = line.rfind('|')
            if pos_end == -1:
                continue
            title = line[pos_end+1:]
            pos_start = line.find('_')
            ids = line[:pos_start]
            if cate_id.has_key(ids):
                tstr = str(cnt)+"\t"+cate_id[ids]+"\t"+title
                cnt += 1
                ofs.write(tstr)


if __name__ == "__main__":
    input = "D:\github\py-nlp-algos\data\samples_all.txt"
    output = "D:\github\py-nlp-algos\data\\samples_norm.txt"
    convert(input, output)