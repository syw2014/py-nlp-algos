#! /usr/bin python
# -*- coding: utf-8 -*-
# File: url_cate_mapping.py
# Author: Jerry.shi
# Date: 2017-03-15 13:16

import sys

# url raw data
url_data = "E:\\work\\Project\\Tasks\\1 TextClassification\\url_label.txt"

# category
cate = "D:\\github\\py-nlp-algos\\data\\category_mapping.txt"

url_feature = "D:\\github\\py-nlp-algos\\result\\url_feature.txt"

with open(cate, 'r') as ifs_cate, open(url_data, 'r') as ifs_url, open(url_feature, 'w') as ofs:
    urls_cateid = {}
    cate_3level = {}
    cate_2level = {}
    cate_1level = {}
    for line in ifs_cate.readlines():
        line = line.strip()
        cxt = line.split("\t")
        if len(cxt) < 2:
            continue
        # 1,store the whole category
        cate_3level[cxt[1].strip()] = cxt[0]
        # 2,only one level labels
        if len(cxt) == 4:
            if not cate_1level.has_key(cxt[3].strip()):
                cate_1level[cxt[3].strip()] = cxt[2]
        elif len(cxt) == 6:
            if not cate_1level.has_key(cxt[5].strip()):
                cate_1level[cxt[5].strip()] = cxt[4]
            if not cate_2level.has_key(cxt[3].strip()):
                cate_2level[cxt[3].strip()] = cxt[2]
    print("Three level category num: %d, \n\t Two level category num: %d, \n\t One level category num: %d") % \
          (len(cate_3level), len(cate_2level), len(cate_1level))


    tmp_urls = []

    forbidden_list = ["www", "com","cn","net","item","detail", "360", "baidu"]
    for url in ifs_url.readlines():
        url = url.strip()
        cxt = url.split("\t")
        if len(cxt) < 2:
            continue
        cate = cxt[1].strip()
        # find in every level category

        url_parse = cxt[0].split(".")
        # print url_parse
        for uu in url_parse:
            if uu not in forbidden_list:
                pre_lens = len(tmp_urls)
                tmp_urls.append(uu)
                tmp_urls = list(set(tmp_urls))
                if len(tmp_urls) > pre_lens:
                    print uu
                    # level 1
                    if cate_1level.has_key(cate):
                        ofs.write(cate_1level[cate] + "\t" + uu)
                        ofs.write("\n")
                        if cxt[0] not in tmp_urls:
                            ofs.write(cate_1level[cate] + "\t" + cxt[0])
                            ofs.write("\n")
                    # level 2
                    if cate_2level.has_key(cate):
                        ofs.write(cate_2level[cate] + "\t" + uu)
                        ofs.write("\n")
                        if cxt[0] not in tmp_urls:
                            ofs.write(cate_2level[cate] + "\t" + cxt[0])
                            ofs.write("\n")
                    # level 3
                    if cate_3level.has_key(cate):
                        ofs.write(cate_3level[cate] + "\t" + uu)
                        ofs.write("\n")
                        if cxt[0] not in tmp_urls:
                            ofs.write(cate_3level[cate] + "\t" + cxt[0])
                            ofs.write("\n")

            # else:
            #     print uu
    print "Process completed!"